package com.zhaojie.swipelayout.interfaces;

public interface SwipeAdapterInterface {

    int getSwipeLayoutResourceId(int position);

    void notifyDatasetChanged();

}
