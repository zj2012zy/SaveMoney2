package com.zhaojie.tablayout;

public interface CustomTabEntity {
    String getTabTitle();

    int getTabSelectedIcon();

    int getTabUnselectedIcon();
}