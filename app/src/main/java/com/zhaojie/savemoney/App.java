package com.zhaojie.savemoney;

import com.orm.SugarContext;
import com.umeng.fb.FeedbackAgent;
import com.umeng.fb.push.FeedbackPush;
import com.zhaojie.commonutils.BaseApplication;
import com.zhaojie.savemoney.data.CostDataManager;

/**
 * Created by zhaojie on 16-1-8.
 */
public class App extends BaseApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        SugarContext.init(this);
        CostDataManager.init(this);

        FeedbackPush.getInstance(this).init(false);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        SugarContext.terminate();
    }
}
