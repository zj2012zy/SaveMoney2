package com.zhaojie.savemoney.model;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by zhaojie on 16-1-3.
 */
public class CostType extends SugarRecord {
    public static final int INPUT_KIND = 1;
    public static final int OUTPUT_KIND = 2;

    public int typeID;
    public int iconID;
    public String name;
    public int kind;
    public boolean editable = false;

    public CostType(int typeID, int iconID, String name, int kind, boolean editable) {
        this.typeID = typeID;
        this.name = name;
        this.iconID = iconID;
        this.kind = kind;
        this.editable = editable;
    }

    public CostType() {

    }

    @Override
    public String toString() {
        return "typeID:" + typeID + ", iconID:" + iconID + ", name:" + name
                + ", kind:" + kind + ", editable:" + editable;
    }

    public List<CostRecord> getCostRecord() {
        return CostRecord.find(CostRecord.class, "type = ?", new String[]{
                String.valueOf(getId())
        });
    }
}
