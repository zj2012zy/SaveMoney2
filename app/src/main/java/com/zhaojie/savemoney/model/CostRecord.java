package com.zhaojie.savemoney.model;

import com.orm.SugarRecord;

/**
 * Created by zhaojie on 16-1-3.
 */
public class CostRecord extends SugarRecord {
    public CostType type;
    public int accountID = 0;
    public double value;
    public String remark;
    public String imagePath;
    public String soundPath;

    public CostRecord() {
    }

    public CostRecord(CostType type, int accountID, double value, String remark, String imagePath, String soundPath) {
        this.type = type;
        this.accountID = accountID;
        this.value = value;
        this.remark = remark;
        this.imagePath = imagePath;
        this.soundPath = soundPath;
    }
}
