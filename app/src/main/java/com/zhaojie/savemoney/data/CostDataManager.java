package com.zhaojie.savemoney.data;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.zhaojie.savemoney.R;
import com.zhaojie.savemoney.model.CostRecord;
import com.zhaojie.savemoney.model.CostType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by zhaojie on 16-1-5.
 */
public class CostDataManager {
    private static final String TAG = "CostDataManager";

    private static final Integer TYPE_ADD = 1;
    private static final Integer TYPE_CLOTH = 2;
    private static final Integer TYPE_DIANYING = 3;
    private static final Integer TYPE_RENQING = 4;
    private static final Integer TYPE_CANYING = 5;
    private static final Integer TYPE_JIANSHEN = 6;
    private static final Integer TYPE_JIAOTONG = 7;
    private static final Integer TYPE_JUJIA = 8;
    private static final Integer TYPE_LINGSHI = 9;
    private static final Integer TYPE_YIYAO = 10;
    private static final Integer TYPE_MEIRONG = 11;
    private static final Integer TYPE_GOUWU = 12;
    private static final Integer TYPE_TONGXUN = 13;
    private static final Integer TYPE_YANJIU = 14;
    private static final Integer TYPE_XUEXI = 15;
    private static final Integer TYPE_RIYONG = 16;
    private static final Integer TYPE_LVYOU = 17;
    private static final Integer TYPE_QITA = 18;
    private static final Integer TYPE_GONGZI = 19;
    private static final Integer TYPE_JIANGJIN = 20;
    private static final Integer TYPE_WAIKUAI = 21;
    private static final Integer TYPE_FENHONG = 22;
    private static final Integer TYPE_BUZHU = 23;
    private static final Integer TYPE_QITASHOURU = 24;
    private static final Integer TYPE_ADDSHOURU = 25;


    private static CostDataManager instance;

    private HashMap<Integer, CostType> typeMaps = new HashMap<>();
    private Context context;

    private CostDataManager(Context context) {
        this.context = context;
        if (getAllCostType() != null && getAllCostType().size() != 0) {
            initCostTypeFromDB();
        } else {
            initCostType();
        }

    }

    public static void init(Context context) {
        if (instance == null) {
            instance = new CostDataManager(context);
        }
    }

    public static CostDataManager getInstance() {
        return instance;
    }

    public List<CostType> getAllCostType() {
        return CostType.listAll(CostType.class);
    }

    public List<CostType> getInputCostType() {
        return CostType.find(CostType.class, "kind=?", String.valueOf(CostType.INPUT_KIND));
    }

    public List<CostType> getOutputCostType() {
        return CostType.find(CostType.class, "kind=?", String.valueOf(CostType.OUTPUT_KIND));
    }

    public List<CostRecord> getAllCostRecord() {
        return CostRecord.listAll(CostRecord.class);
    }

    public List<CostRecord> getOutputCostRecord() {
        List<CostType> outputTypes = getOutputCostType();
        List<CostRecord> costRecords = new ArrayList<>();
        for (CostType type : outputTypes) {
            costRecords.addAll(type.getCostRecord());
        }
        return costRecords;
    }

    public List<CostRecord> getInputCostRecord() {
        List<CostType> outputTypes = getInputCostType();
        List<CostRecord> costRecords = new ArrayList<>();
        for (CostType type : outputTypes) {
            costRecords.addAll(type.getCostRecord());
        }
        return costRecords;
    }

    public double getTotalOutputValue() {
        List<CostRecord> outputCostRecord = getOutputCostRecord();
        double total = 0;
        for (CostRecord record : outputCostRecord) {
            total += record.value;
        }
        return total;
    }

    public double getTotalInputValue() {
        List<CostRecord> inputCostRecord = getInputCostRecord();
        double total = 0;
        for (CostRecord record : inputCostRecord) {
            total += record.value;
        }
        return total;
    }

    private void initCostType() {
        Log.d(TAG, "initCostType()");
        typeMaps.put(TYPE_ADD, new CostType(TYPE_ADD, R.drawable.type_bg_add, context.getString(R.string.add), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_CLOTH, new CostType(TYPE_CLOTH, R.drawable.type_bg_cloth, context.getString(R.string.cloth), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_DIANYING, new CostType(TYPE_DIANYING, R.drawable.type_bg_dianying, context.getString(R.string.dianying), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_RENQING, new CostType(TYPE_RENQING, R.drawable.type_bg_guanxi, context.getString(R.string.renqing), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_CANYING, new CostType(TYPE_CANYING, R.drawable.type_bg_hamburger, context.getString(R.string.canying), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_JIANSHEN, new CostType(TYPE_JIANSHEN, R.drawable.type_bg_jianshen, context.getString(R.string.jianshen), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_JIAOTONG, new CostType(TYPE_JIAOTONG, R.drawable.type_bg_jiaotong, context.getString(R.string.jiaotong), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_JUJIA, new CostType(TYPE_JUJIA, R.drawable.type_bg_jujia, context.getString(R.string.jujia), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_LINGSHI, new CostType(TYPE_LINGSHI, R.drawable.type_bg_lingshi, context.getString(R.string.lingshi), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_YIYAO, new CostType(TYPE_YIYAO, R.drawable.type_bg_yiyao, context.getString(R.string.yiyao), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_MEIRONG, new CostType(TYPE_MEIRONG, R.drawable.type_bg_meirong, context.getString(R.string.meirong), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_GOUWU, new CostType(TYPE_GOUWU, R.drawable.type_bg_shopping, context.getString(R.string.gouwu), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_TONGXUN, new CostType(TYPE_TONGXUN, R.drawable.type_bg_phone, context.getString(R.string.tongxun), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_YANJIU, new CostType(TYPE_YANJIU, R.drawable.type_bg_yanjiu, context.getString(R.string.yanjiu), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_XUEXI, new CostType(TYPE_XUEXI, R.drawable.type_bg_study, context.getString(R.string.xuexi), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_RIYONG, new CostType(TYPE_RIYONG, R.drawable.type_bg_riyong, context.getString(R.string.riyong), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_LVYOU, new CostType(TYPE_LVYOU, R.drawable.type_bg_tour, context.getString(R.string.lvyou), CostType.OUTPUT_KIND, false));
        typeMaps.put(TYPE_QITA, new CostType(TYPE_QITA, R.drawable.type_bg_other, context.getString(R.string.qita), CostType.OUTPUT_KIND, false));

        typeMaps.put(TYPE_GONGZI, new CostType(TYPE_GONGZI, R.drawable.gongzi, context.getString(R.string.gongzi), CostType.INPUT_KIND, false));
        typeMaps.put(TYPE_JIANGJIN, new CostType(TYPE_JIANGJIN, R.drawable.jiangjin, context.getString(R.string.jiangjin), CostType.INPUT_KIND, false));
        typeMaps.put(TYPE_WAIKUAI, new CostType(TYPE_WAIKUAI, R.drawable.waikuai, context.getString(R.string.waikuai), CostType.INPUT_KIND, false));
        typeMaps.put(TYPE_FENHONG, new CostType(TYPE_FENHONG, R.drawable.fenhong, context.getString(R.string.fenhong), CostType.INPUT_KIND, false));
        typeMaps.put(TYPE_BUZHU, new CostType(TYPE_BUZHU, R.drawable.buzhu, context.getString(R.string.buzhu), CostType.INPUT_KIND, false));
        typeMaps.put(TYPE_QITASHOURU, new CostType(TYPE_QITASHOURU, R.drawable.other_e, context.getString(R.string.qitashouru), CostType.INPUT_KIND, false));
        typeMaps.put(TYPE_ADDSHOURU, new CostType(TYPE_ADDSHOURU, R.drawable.add_e, context.getString(R.string.zengjia), CostType.INPUT_KIND, false));

        for (CostType item : typeMaps.values()) {
            Log.d(TAG, "item:" + item);
            item.save();
        }
    }

    private void initCostTypeFromDB() {
        List<CostType> items = getAllCostType();
        for (CostType item : items) {
            typeMaps.put(item.typeID, item);
        }
    }
}
