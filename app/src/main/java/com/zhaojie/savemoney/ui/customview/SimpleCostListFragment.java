package com.zhaojie.savemoney.ui.customview;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.zhaojie.savemoney.R;
import com.zhaojie.savemoney.ui.CostListAdapter;
import com.zhaojie.swipelayout.util.Attributes;

import butterknife.Bind;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class SimpleCostListFragment extends Fragment {

    @Bind(R.id.cost_list)
    ListView costList;

    private CostListAdapter mAdapter;
    private String title;
    private int typeKind;

    public static SimpleCostListFragment getInstance(String title, int typeKind) {
        SimpleCostListFragment sf = new SimpleCostListFragment(title, typeKind);
        return sf;
    }

    public SimpleCostListFragment() {
        // Required empty public constructor
    }

    private SimpleCostListFragment(String title, int typeKind){
        this.title = title;
        this.typeKind = typeKind;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.notifyDataChanged(typeKind);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_simple_list, null);
        ButterKnife.bind(this, v);

        mAdapter = new CostListAdapter(getActivity(), typeKind);
        costList.setAdapter(mAdapter);
        mAdapter.setMode(Attributes.Mode.Single);
        costList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RelativeLayout costDetail = (RelativeLayout) view.findViewById(R.id.cost_detail);
                if (costDetail.getVisibility() == View.GONE) {
                    costDetail.setVisibility(view.VISIBLE);
                } else {
                    costDetail.setVisibility(View.GONE);
                }

            }
        });
        costList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.e("ListView", "OnTouch");
                return false;
            }
        });
        costList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "OnItemLongClickListener", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        costList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                Log.e("ListView", "onScrollStateChanged");
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        costList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("ListView", "onItemSelected:" + position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.e("ListView", "onNothingSelected:");
            }
        });


        return v;
    }
}