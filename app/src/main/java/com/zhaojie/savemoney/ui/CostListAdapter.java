package com.zhaojie.savemoney.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zhaojie.savemoney.R;
import com.zhaojie.savemoney.data.CostDataManager;
import com.zhaojie.savemoney.model.CostRecord;
import com.zhaojie.savemoney.model.CostType;
import com.zhaojie.swipelayout.SimpleSwipeListener;
import com.zhaojie.swipelayout.SwipeLayout;
import com.zhaojie.swipelayout.adapters.BaseSwipeAdapter;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CostListAdapter extends BaseSwipeAdapter {
    @Bind(R.id.swipe)
    SwipeLayout swipeLayout;
    @Bind(R.id.cost_icon)
    ImageView costIcon;
    @Bind(R.id.cost_value)
    TextView costValue;

    private Context mContext;
    private List<CostRecord> costRecords = new ArrayList<>();

    public CostListAdapter(Context mContext, int typeKind) {
        this.mContext = mContext;
        initCostRecords(typeKind);
    }

    public void notifyDataChanged(int typeKind) {
        initCostRecords(typeKind);
        notifyDataSetChanged();
    }

    private void initCostRecords(int typeKind) {
        if (typeKind == CostType.INPUT_KIND) {
            costRecords = CostDataManager.getInstance().getInputCostRecord();
        } else if (typeKind == CostType.OUTPUT_KIND) {
            costRecords = CostDataManager.getInstance().getOutputCostRecord();
        } else {
            costRecords = CostDataManager.getInstance().getAllCostRecord();
        }

    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.listview_item, null);
        ButterKnife.bind(this, v);
        initEvent();
        return v;
    }

    private void initEvent() {
        swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.trash));
            }
        });
        swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
            @Override
            public void onDoubleClick(SwipeLayout layout, boolean surface) {
                Toast.makeText(mContext, "DoubleClick", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void fillValues(int position, View convertView) {
        CostRecord costRecord = costRecords.get(position);
        costIcon.setImageResource(costRecord.type.iconID);
        costValue.setText(costRecord.type.name + ":" + costRecord.value);
    }

    @Override
    public int getCount() {
        return costRecords.size();
    }

    @Override
    public Object getItem(int position) {
        return costRecords.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
