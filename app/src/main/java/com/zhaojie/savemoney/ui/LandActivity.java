package com.zhaojie.savemoney.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.zhaojie.savemoney.App;
import com.zhaojie.savemoney.R;

public class LandActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_land);

        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(LandActivity.this, MainActivity.class));
                        finish();
                    }
                }
        , 1500);
    }
}
