package com.zhaojie.savemoney.ui;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zhaojie.savemoney.R;
import com.zhaojie.savemoney.data.CostDataManager;
import com.zhaojie.savemoney.model.CostType;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by zhaojie on 16-1-10.
 */
public class CostTypeAdapter extends BaseAdapter {
    private static final String TAG = "CostTypeAdapter";
    private LayoutInflater mInflater;

    List<CostType> items = new ArrayList<>();

    public CostTypeAdapter(Context context, int typeKind) {
        if (typeKind == CostType.INPUT_KIND) {
            items = CostDataManager.getInstance().getInputCostType();
        } else {
            items = CostDataManager.getInstance().getOutputCostType();
        }

        Log.d(TAG, "items.size:" + items.size());
        this.mInflater = LayoutInflater.from(context);
    }

    public void setTypeKind(int typeKind) {
        items.clear();
        if (typeKind == CostType.INPUT_KIND) {
            items = CostDataManager.getInstance().getInputCostType();
        } else {
            items = CostDataManager.getInstance().getOutputCostType();
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.costtype_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        CostType item = items.get(position);
        if (null != item) {
            holder.typeIcon.setImageResource(item.iconID);
            holder.typeName.setText(item.name);
            holder.costType = item;
        }

        return convertView;
    }

    public class ViewHolder {
        @Bind(R.id.type_icon)
        ImageView typeIcon;
        @Bind(R.id.type_name)
        TextView typeName;

        CostType costType;
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
